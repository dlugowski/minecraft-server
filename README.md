Minecraft Server setup:

1. ssh to the server and update package manager:
`sudo apt update`
`sudo apt upgrade`

2. move to the working directory for minecraft:
`cd /usr/local`
`mkdir minecraft`
`cd minecraft`

3. clone minecraft repo and move into repo directory:
`git clone https://gitlab.com/dlugowski/minecraft-server.git`
`cd minecraft-server/`

4. run makefile to setup the minecraft server:
`sudo make minecraft`

