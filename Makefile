minecraft: user permissions java minecraft-download-server
	test -f eula.txt || ${MAKE} eula
	sudo cp systemd/minecraft.service /etc/systemd/system/minecraft.service
	sudo systemctl daemon-reload
	sudo systemctl restart minecraft
	sudo systemctl enable minecraft
	${MAKE} minecraft-restart

eula:
	sudo touch eula.txt
	@sudo chmod 666 eula.txt
	echo "eula=true" > eula.txt
	@sudo chmod 644 eula.txt

minecraft-download-server:
	test -f server.jar || sudo wget https://launcher.mojang.com/v1/objects/1b557e7b033b583cd9f66746b7a9ab1ec1673ced/server.jar

minecraft-restart:
	# add cron job to restart the server at 5am MST
	test -f .crontab_added || echo "0 11 * * * minecraft /usr/local/src/minecraft/systemd/server_restart.sh" | sudo tee -a /etc/crontab
	sudo touch .crontab_added

java: 
	@if java -version > /dev/null; then \
		echo "java is already installed"; \
	else \
		echo "java is not installed... installing java"; \
		sudo apt -y install openjdk-17-jre-headless; \
	fi


user: 
	-sudo useradd -r -s /bin/false minecraft

permissions: user
	sudo chown -R minecraft:minecraft ./

discord:
	cp /usr/local/src/minecraft/systemd/discord.service /etc/systemd/system/discord.service
	systemctl daemon-reload
	systemctl restart discord
	systemctl enable discord
