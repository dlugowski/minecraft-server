#!/bin/sh


# this script is run at 4 am daily by a cron job.
# this script restarts the minecraft server.

/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say IT IS 4 AM. YOU SHOULD BE ALSEEP..."\015'
/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say SERVER RESTARTING IN 5 MINUTES..."\015'
/bin/sleep 60
/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say SERVER RESTARTING IN 4 MINUTES..."\015'
/bin/sleep 60
/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say SERVER RESTARTING IN 3 MINUTES..."\015'
/bin/sleep 60
/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say SERVER RESTARTING IN 2 MINUTES..."\015'
/bin/sleep 60
/usr/bin/screen -p 0 -S mc-server -X eval 'stuff "say SERVER RESTARTING IN 1 MINUTES..."\015'
/bin/sleep 45

sudo systemctl restart minecraft
