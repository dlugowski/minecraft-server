#!/bin/bash


# example of message to match with regular expression:
# [06:41:16] [Server thread/INFO]: FireMaster1294 left the game


#x=$(grep -Po '(?<=\[Server thread/INFO\]: )[^]> ]+ left the game$' latest.log) && echo ${x}
# tail -f ../minecraft/logs/latest.log | grep --line-buffered -P  '^[^z]*$'

webhook=`cat webhook.txt`

while true
do
	tail -f -n 0 /usr/local/src/minecraft/logs/latest.log |  grep --line-buffered -Po '(?<=\[Server thread/INFO\]: )[^]> ]+( left the game$| joined the game$)' | xargs -I {} curl -X POST -H "Content-Type: application/json" -d '{"username":"minecraft_logins","content":"{}"}' $webhook


done
