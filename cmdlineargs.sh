#!/bin/bash

/usr/bin/java -Xms1024M -Xmx1024M -XX:+UseG1GC  -XX:+CMSClassUnloadingEnabled -XX:ParallelGCThreads=2 -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10  -jar /usr/local/src/minecraft/minecraft_server-1.16.5.jar nogui
